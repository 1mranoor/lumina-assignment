require("dotenv").config();
const express = require('express');
const cors = require('cors');
const mysql = require('mysql');

const PORT = process.env.REACT_APP_PORT || 5004;
const app = express();
app.use(cors());
app.use(express.json());

// REGISTER
app.post('/register', (req, res) => {
    const {firstName, lastName} = req.body;
    try{
        // db.query(`INSERT INTO users (username, password) VALUES ('${username}', '${password}')`
        db.query(`
            IF NOT EXISTS (SELECT * FROM users 
               (SELECT * FROM users WHERE firstName = '${firstName}')
            BEGIN
                (INSERT INTO users (firstName, lastName) VALUES ('${firstName}', '${lastName}'))
            END`, 
            (err, result) => {
            err?res.send(err):res.send(result);
        });
    } catch(err){
        console.log(err);
    }
});

// LOGIN
app.post('/login', (req, res) => {
    const {firstName, lastName} = req.body;
    try{
        db.query('SELECT * FROM users WHERE firstName = ? AND lastName = ?', [firstName, lastName],(err, result) => {
            if(err)res.send({err: err});
            if(result.length>0){res.send(result)}else{res.send({message: 'Invalid username or password'});}
        });
    } catch(err){
        console.log(err);
    }
});

app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});

// CONNECT TO DB
const db = mysql.createConnection({
    host:process.env.REACT_APP_HOST,
    user: process.env.REACT_APP_USER,
    password: process.env.REACT_APP_PASSWORD,
    database: process.env.REACT_APP_DATABASE
});

db.connect((err) => {
    if(err) throw err;
    console.log("Connected to database!");
})


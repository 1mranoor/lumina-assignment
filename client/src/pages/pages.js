export {Moviepage} from './Moviepage'
export {SelectedMovie} from './SelectedMovie'
export {LoginRegister} from './LoginRegister'
export {UserSettings} from './UserSettings'
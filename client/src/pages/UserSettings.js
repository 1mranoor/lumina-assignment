import {Link} from 'react-router-dom';

export const UserSettings = ({logout}) => {
    return (
        <div>
            <Link to="/moviepage">Movie Page</Link>
            <h1>You are LOGGED IN!</h1>
            <button onClick={logout}>Logout</button>
        </div>
    )
}

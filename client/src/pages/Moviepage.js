import {useState, useEffect} from 'react';
// import axios from 'axios'
import "bootstrap/dist/css/bootstrap.min.css";
import "../App.css";

import {MovieList, MovieListHeading, SearchBox, AddFavourite, RemoveFavourite, Header} from '../components/components';

export const Moviepage = ({user, favourites, setFavourites}) => {
    const APIKEY = process.env.REACT_APP_APIKEY;
    const [movies, setMovies] = useState([]);
    const [searchValue, setSearchValue] = useState('');
    const preloadedMovies = ['Avengers', 'Superman', 'spider', 'Scooby', 'Naruto'];

    const getMovieRequest = async (search = `&s=${searchValue}`) => {
        try{
            // const search = `&s=${searchValue}`;
            const response = await fetch(APIKEY+search);
            const responseJson = await response.json();
            // console.log(responseJson);
            
            if(responseJson.Search)setMovies(responseJson.Search);
        }catch(error){
            console.log(error);
        }
    }
    
    const moviesStringToJSON = () => {
        if(typeof favourites == 'string'){
            let stringFavourites = favourites.split(',');
            setFavourites(null);

            for(let i = 0; i < stringFavourites.length; i++){
                movieIdToJSON(stringFavourites[i]);
            }
        }   
    }
    const movieIdToJSON = async (movieId) => {
        let response = await fetch (APIKEY+`&i=${movieId}`);
        let responseJson = await response.json();
        setFavourites(...favourites, responseJson);
    }

    useEffect(() => {
        // if search is blank, pull up movies of avengers
        if(searchValue.length === 0){
            let randomMovie = preloadedMovies[Math.floor(Math.random() * preloadedMovies.length)];
            getMovieRequest(`&s=${randomMovie}`);
        }else{
            getMovieRequest();
        }
        
    }, [searchValue]);

    // onLoad
    useEffect(() => {
        // if search is blank, pull up movies of avengers
        // set movies to json object of movies using id
        // console.log("running st2js function: BEFORE")
        // console.log(favourites)
        moviesStringToJSON();
        // console.log("running st2js function: AFTTER")
        // console.log(favourites)
        console.log("fav: ", JSON.stringify(favourites));
        console.log("fav2: ", favourites);
        
    }, []);

    const toggleFavouriteMovie = (movie) => {

        if(favourites.map((fav) => fav.imdbID).includes(movie.imdbID)){
            // already found in favourites -> remove it from favourites
            const newFavList = favourites.filter((fav) => fav.imdbID !== movie.imdbID);
            setFavourites(newFavList);
        } else {
            // didn't find in favourites -> add it to favourites
            const newFavList = [...favourites, movie];
            setFavourites(newFavList);
        }
    }

    return (
        <>
        {user && (
        <div className='container-fluid movie-app'>
            <div className='row d-flex align-items-center mt-4 mb-4'>
                <Header user={user}/>
            </div>
            <hr/>
            <div className='row d-flex align-items-center mt-4 mb-4'>
                <MovieListHeading heading='Favourites'/>
            </div>
            <div className='row'>
                {console.log(favourites)}
                <MovieList movies={favourites} favouriteComponent={RemoveFavourite} handleFavouriteClick={toggleFavouriteMovie}/>
            </div>
            
            <div className='row d-flex align-items-center mt-4 mb-4'>
                <MovieListHeading heading='Movies'/>
                <SearchBox searchValue={searchValue} setSearchValue={setSearchValue}/>
            </div>
            <div className='row'>
                <MovieList movies={movies} favouriteComponent={AddFavourite} handleFavouriteClick={toggleFavouriteMovie}/>
            </div>
            <hr/>
        </div>
        )}
        </>
    )
}

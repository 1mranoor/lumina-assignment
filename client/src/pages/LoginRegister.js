import {useState } from 'react';
import axios from 'axios';
import { useNavigate } from "react-router-dom";

export const LoginRegister = ({authenticate, user, setUser}) => {

    const DB_PATH = process.env.REACT_APP_DBPATH || "";
    const navigate = useNavigate();

    const handleLogin = () => {
        authenticate(() => navigate("/profile"));
    }

    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [loginStatus, setLoginStatus] = useState('');

    const register = () => {
        axios.post(DB_PATH + '/register', {
            firstName: firstName, lastName: lastName}).then(res => {
                console.log(res);
        });
   }

    const login = () => {
        // axios.post(DB_PATH + '/login',{
        axios.post('http://localhost:5001/login',{   
        firstName: firstName, lastName: lastName}).then(res => {
            // res.data.message?setLoginStatus(res.data.message):setLoginStatus(res.data);
            // setUser(JSON.parse(res.data));
            // let test1 = JSON.parse(res.data);
            console.log(res.data[0]);
            setUser(res.data[0]);
        });
    }

    const guest = () => {
      
    }

    return (
        <div>
            <h2>Please login/ register</h2>
            <button onClick={handleLogin}>Login</button>
            <div className="login-register">
                <h1>Login/ Register</h1>
                <label style={{'marginRight':'2vw'}}>Firstname</label>
                <input type="text" placeholder="Firstname..." onChange={(e)=>{setFirstName(e.target.value)}}/>
                <br/>
                <label style={{'marginRight':'2vw'}}>Lastname</label>
                <input type="text" placeholder="Lastname..." onChange={(e)=>{setLastName(e.target.value)}}/>
                <br/>
                <button style={{'marginRight':'2vw', 'marginTop': '10px'}} onClick={login}>Login</button>
                <button style={{'marginLeft':'2vw', 'marginTop': '10px'}} onClick={register}>Register</button>
                <button style={{'marginLeft':'2vw', 'marginTop': '10px'}} onClick={guest}>Continue as Guest</button>
            </div>
            <h1>{loginStatus}</h1>
        </div>
    )
}

function SearchBox({value, searchValue, setSearchValue}) {
    return (

        <div className="col col-sm-4 d-flex">
            <input 
                type="text" 
                className="form-control" 
                placeholder="Search..."
                onChange={(event) => setSearchValue(event.target.value)}
            />
        </div> 
    )
}

export default SearchBox;

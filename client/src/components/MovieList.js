import React from 'react'

function MovieList({movies, favouriteComponent, handleFavouriteClick, unfavouriteComponent}) {
    const FavouriteComponent = favouriteComponent;
    return (
        <>
            {typeof movies === 'object' && (
                movies.map((movie, index) => 
                <div className='image-container justify-content-start m-3' key={index}>
                    <img src={movie.Poster} alt=""/>
                    <div className='movie-info'>
                    <p>
                        {/* {movie.Year}:  */}
                    {movie.Title}</p>
                    <p></p>
                    </div>
                    <div className='overlay d-flex align-items-center justify-content-center' onClick={() => handleFavouriteClick(movie)}>
                    <FavouriteComponent/>
                    </div>
                </div>
            ))}
        </>
    )
}

export default MovieList;

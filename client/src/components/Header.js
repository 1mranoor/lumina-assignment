import { Links } from 'react-router-dom'
 
const Header = ({user}) => {
    return (
        <nav className="navbar d-flex header-links">
            <div className='navbar-brand greetings'>
                Hello <b>{user=='guest'? 'guest' : user.firstName}</b>!
            </div>
        
            {user=='guest' ? (
                <div className='loginlogout login-register'>
                    <a>Login/ Register</a>
                </div>
                ):(
                <div className='loginlogout logout'>
                    <a>Logout</a>
                </div>
            )}
        </nav>
    )
}
export default Header;
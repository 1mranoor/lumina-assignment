import {useState, useEffect} from 'react';
import {Routes, Route, Navigate} from 'react-router-dom';
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";

import {Moviepage, SelectedMovie, LoginRegister, UserSettings} from './pages/pages';

function App() {

  const [user, setUser] = useState('guest');
  const [favourites, setFavourites] = useState([]);
    // if we have user in LS, fetch user
    useEffect(() => {
        const u = localStorage.getItem('user');
        // u && JSON.parse(u) ? setUser(true) : setUser(false);
        if (u && Object.keys(JSON.parse(u)).length>1){
          setUser(JSON.parse(u))
        }else{
          setUser('guest');
        }

        // copy over the favourites from LS
        if (user !== 'guest') {
            setFavourites(user.favourite_movies);
        }
    }, []);

    // if we login/ logout, set user in LS
    useEffect(() => {
        localStorage.setItem('user', JSON.stringify(user));
    }, [user]);

  return (
    <Routes>
        {!user && (
            <Route path="/loginregister" element={<LoginRegister authenticate={() => setUser(true)} user={user} setUser={setUser}/>}/>
        )}
        {user && (<>
            <Route path="/moviepage" element={<Moviepage user={user} favourites={favourites} setFavourites={setFavourites}/>}/>
            <Route path="/usersettings" element={<UserSettings logout={() => setUser(false)}/>}/>
        </>)}

        <Route path="*" element={<Navigate to={user ? "/moviepage" : "/loginRegister"}/>} />
    </Routes>
  );
}

export default App;
